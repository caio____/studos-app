# Studos App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.4.

## Prerequisites

This project will be made by use 'Ubuntu Server 16.04 LTS' Linux distribution and Npm 3.10.10.

***

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Built With

* [Angular](https://angular.io/) - Angular Javascript Framework
* [ZURB Foundation](https://foundation.zurb.com/) - Foundation Responsive Front-end Framework
* [TypeScript](https://www.typescriptlang.org/) - Javascript Preprocessor Framework
* [Font Awesome](http://fontawesome.io/) -  The iconic font and CSS toolkit
* [NPM](https://www.npmjs.com/) - Package Manager
