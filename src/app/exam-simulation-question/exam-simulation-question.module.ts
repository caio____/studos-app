import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';

import { ExamSimulationQuestionComponent } from './exam-simulation-question.component';
import { SharedModule } from '../shared';

const examSimulationQuestionRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: ':examId/simulation/:simulationId',
    component: ExamSimulationQuestionComponent
  }
]);

@NgModule({
  imports: [
    examSimulationQuestionRouting,
    SharedModule,
    ChartsModule
  ],
  declarations: [
    ExamSimulationQuestionComponent
  ]
})

export class ExamSimulationQuestionModule {}
