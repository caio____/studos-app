import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExamSimulationQuestionService } from '../shared/services/exam-simulation-question.service';

import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'exam-simulation-question-page',
  templateUrl: './exam-simulation-question.component.html',
  styleUrls: ['./exam-simulation-question.component.scss']
})

export class ExamSimulationQuestionComponent implements OnInit, OnDestroy {

  private routeSub: any;

  public forms: Array<any> = [];
  public stepActive: number = 0;

  public examResult: any = {
    execTime: '',
    questions: {total: 0, falts: 0, hits: 0}
  };

  public answersCollection: Array<any> = [];
  public examSimulationQuestionCollection: Array<any> = [];

  public startAt: Date = new Date();
  public finish: boolean = false;

  public barChartType: string = 'bar';
  public barChartLabels: string[] = [];
  public barChartLegend: boolean = false;

  public barChartOptions: Object = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public barChartData: Array<any> = [
    {data: [], label: ''},
    {data: [], label: ''}
  ];

  constructor(
    private activeRoute: ActivatedRoute,
    private ExamSimulationQuestionService: ExamSimulationQuestionService
  ) {}

  ngOnInit() {
    this.routeSub = this.activeRoute.params.subscribe(params => {

      this.ExamSimulationQuestionService
        .fetchAll(+params['examId'], +params['simulationId'])
        .subscribe(res => {

          this.examSimulationQuestionCollection = res;

          for (let i in res) {
            this.forms[i] = new FormGroup({
              answer: new FormControl('', Validators.required)
            });
          }

          this.barChartLabels = this.examSimulationQuestionCollection.map(i => {
            return i.exam_simulation_question_discipline.name
          });

        });
    });
  }

  onFormSubmit(index: number) {

    let form     = this.forms[index];
    let question = this.examSimulationQuestionCollection[index];

    let answer   = question.exam_simulation_question_answers[form.controls['answer'].value];
    answer['exam_simulation_question_discipline'] = question.exam_simulation_question_discipline;

    this.answersCollection.push(answer);

    if (index >= (this.forms.length - 1)) {

      this.examResult = {
        execTime: this.stopCountDown(),
        questions: {
          total: this.examSimulationQuestionCollection.length,
          falts: this.answersCollection.filter(i => {return !i.correct ? 1 : 0}).length,
          hits: this.answersCollection.filter(i => {return i.correct ? 1 : 0}).length
        }
      };

      this.barChartData = [
        {label: 'Erradas', data: this.answersCollection.map(i => {return !i.correct ? 1 : 0})},
        {label: 'Corretas', data: this.answersCollection.map(i => {return i.correct ? 1 : 0})}
      ];

      this.finish = true;
    }

    this.stepActive++;
  }

  stopCountDown() {

    let days, hours, minutes, seconds, t;

    t = (new Date().getTime() - this.startAt.getTime()) / 1000;

    days = Math.floor(t / 86400);
    t -= days * 86400;

    hours = Math.floor(t / 3600) % 24;
    t -= hours * 3600;

    minutes = Math.floor(t / 60) % 60;
    t -= minutes * 60;

    seconds = Math.round(t % 60);

    return [hours + 'h', minutes + 'm', seconds + 's'].join(' ');
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
