import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ExamSimulationService} from '../shared/services/exam-simulation.service';

@Component({
  selector: 'exam-simulation-page',
  templateUrl: './exam-simulation.component.html'
})

export class ExamSimulationComponent implements OnInit, OnDestroy {

  private routeSub: any;
  public examSimulationCollection: Array<any> = [];

  constructor(
    private route: ActivatedRoute,
    private ExamSimulationService: ExamSimulationService
  ) {}

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.ExamSimulationService
        .fetchAll(+params['examId'])
        .subscribe(res => {
          this.examSimulationCollection = res
        });
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
