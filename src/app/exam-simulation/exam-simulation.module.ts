import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ExamSimulationComponent } from './exam-simulation.component';
import { SharedModule } from '../shared';

const examSimulationRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: ':examId/simulation',
    component: ExamSimulationComponent
  }
]);

@NgModule({
  imports: [
    examSimulationRouting,
    SharedModule
  ],
  declarations: [
    ExamSimulationComponent
  ]
})

export class ExamSimulationModule {}
