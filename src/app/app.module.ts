import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ExamModule } from './exam/exam.module';
import { ExamSimulationModule } from './exam-simulation/exam-simulation.module';
import { ExamSimulationQuestionModule } from './exam-simulation-question/exam-simulation-question.module';

import {
  HttpClientService,
  FooterComponent,
  HeaderComponent,
  ExamService,
  ExamSimulationService,
  ExamSimulationQuestionService,
  SharedModule
} from './shared';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([], { useHash: false });

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent
  ],
  imports: [
    rootRouting,
    BrowserModule,
    ExamModule,
    ExamSimulationModule,
    ExamSimulationQuestionModule,
    SharedModule
  ],
  providers: [
    HttpClientService,
    ExamService,
    ExamSimulationService,
    ExamSimulationQuestionService,
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
