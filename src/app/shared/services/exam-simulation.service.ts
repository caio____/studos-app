import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { HttpClientService } from './http-client.service';
import { ExamSimulation } from '../models/exam-simulation.model';

@Injectable()

export class ExamSimulationService {

  constructor (
    private httpClientService: HttpClientService
  ) {}

  fetchAll(examId): Observable<ExamSimulation[]> {
    return this.httpClientService.get(`/v1/exam/${examId}/simulation/`)
      .map((res:Response) => res)
      .catch((error:any) => Observable.throw(error.message || 'Server error'));
  }

  fetchOne(examId, simulationId): Observable<ExamSimulation> {
    return this.httpClientService.get(`/v1/exam/${examId}/simulation/${simulationId}`)
      .map((res:Response) => res)
      .catch((error:any) => Observable.throw(error.message || 'Server error'));
  }

}
