import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { HttpClientService } from './http-client.service';
import { Exam } from '../models';

@Injectable()

export class ExamService {

  constructor (
    private httpClientService: HttpClientService
  ) {}

  fetchAll(): Observable<Exam[]> {
    return this.httpClientService.get('/v1/exam/')
      .map((res:Response) => res)
      .catch((error:any) => Observable.throw(error.message || 'Server error'));
  }

  fetchOne(examId): Observable<Exam> {
    return this.httpClientService.get(`/v1/exam/${examId}`)
      .map((res:Response) => res)
      .catch((error:any) => Observable.throw(error.message || 'Server error'));
  }

}
