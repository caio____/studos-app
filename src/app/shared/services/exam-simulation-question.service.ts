import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { HttpClientService } from './http-client.service';
import { ExamSimulationQuestion } from '../models/exam-simulation-question.model';

@Injectable()

export class ExamSimulationQuestionService {

  constructor (
    private httpClientService: HttpClientService
  ) {}

  fetchAll(examId, simulationId): Observable<ExamSimulationQuestion[]> {
    return this.httpClientService.get(`/v1/exam/${examId}/simulation/${simulationId}/question`)
      .map((res:Response) => res)
      .catch((error:any) => Observable.throw(error.message || 'Server error'));
  }

  fetchOne(examId, simulationId, questionId): Observable<ExamSimulationQuestion> {
    return this.httpClientService.get(`/v1/exam/${examId}/simulation/${simulationId}/question/${questionId}`)
      .map((res:Response) => res)
      .catch((error:any) => Observable.throw(error.message || 'Server error'));
  }

}
