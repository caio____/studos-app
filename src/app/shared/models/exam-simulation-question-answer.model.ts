export class ExamSimulationQuestionAnswer {
  constructor(
    public id: string,
    public answer: string,
    public correct: boolean,
    public createdby: number,
    public updatedby: number,
    public createdat: string,
    public updatedat: string,
    public enabled: boolean
  ){}
}
