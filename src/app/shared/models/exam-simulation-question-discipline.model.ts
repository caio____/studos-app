export class ExamSimulationQuestionDiscipline {
  constructor(
    public id: string,
    public name: string,
    public createdby: number,
    public updatedby: number,
    public createdat: string,
    public updatedat: string,
    public enabled: boolean
  ){}
}
