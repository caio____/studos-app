export class ExamSimulation {
  constructor(
    public id: string,
    public name: string,
    public version: string,
    public createdby: number,
    public updatedby: number,
    public createdat: string,
    public updatedat: string,
    public enabled: boolean
  ){}
}
