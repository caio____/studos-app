import { ExamSimulationQuestionAnswer } from './exam-simulation-question-answer.model';
import { ExamSimulationQuestionDiscipline } from './exam-simulation-question-discipline.model';

export class ExamSimulationQuestion {
  constructor(
    public id: string,
    public question: string,
    public answer_type: string,
    public createdby: number,
    public updatedby: number,
    public createdat: string,
    public updatedat: string,
    public enabled: boolean,
    public exam_simulation_question_discipline: ExamSimulationQuestionDiscipline,
    public exam_simulation_question_answers: ExamSimulationQuestionAnswer[]
  ){}
}
