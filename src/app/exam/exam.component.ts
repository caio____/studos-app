import { Component, OnInit } from '@angular/core';
import { ExamService } from '../shared/services/exam.service';

@Component({
  selector: 'exam-page',
  templateUrl: './exam.component.html'
})

export class ExamComponent implements OnInit {

  public examCollection: Array<any> = [];

  constructor(
    private ExamService: ExamService
  ) {}

  ngOnInit() {
    this.ExamService
      .fetchAll()
      .subscribe(res => {
        this.examCollection = res
      });
  }
}
