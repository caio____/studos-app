import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ExamComponent } from './exam.component';
import { SharedModule } from '../shared';

const examRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: '',
    component: ExamComponent
  }
]);

@NgModule({
  imports: [
    examRouting,
    SharedModule
  ],
  declarations: [
    ExamComponent
  ]
})

export class ExamModule {}
