webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<layout-header></layout-header>\n<router-outlet></router-outlet>\n<layout-footer></layout-footer>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html")
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__exam_exam_module__ = __webpack_require__("../../../../../src/app/exam/exam.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__exam_simulation_exam_simulation_module__ = __webpack_require__("../../../../../src/app/exam-simulation/exam-simulation.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__exam_simulation_question_exam_simulation_question_module__ = __webpack_require__("../../../../../src/app/exam-simulation-question/exam-simulation-question.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared__ = __webpack_require__("../../../../../src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var rootRouting = __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */].forRoot([], { useHash: false });
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__shared__["d" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_7__shared__["e" /* HeaderComponent */]
            ],
            imports: [
                rootRouting,
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4__exam_exam_module__["a" /* ExamModule */],
                __WEBPACK_IMPORTED_MODULE_5__exam_simulation_exam_simulation_module__["a" /* ExamSimulationModule */],
                __WEBPACK_IMPORTED_MODULE_6__exam_simulation_question_exam_simulation_question_module__["a" /* ExamSimulationQuestionModule */],
                __WEBPACK_IMPORTED_MODULE_7__shared__["g" /* SharedModule */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__shared__["f" /* HttpClientService */],
                __WEBPACK_IMPORTED_MODULE_7__shared__["a" /* ExamService */],
                __WEBPACK_IMPORTED_MODULE_7__shared__["c" /* ExamSimulationService */],
                __WEBPACK_IMPORTED_MODULE_7__shared__["b" /* ExamSimulationQuestionService */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/exam-simulation-question/exam-simulation-question.component.html":
/***/ (function(module, exports) {

module.exports = "<main class=\"grid-container\">\r\n  <section>\r\n    <div class=\"grid-x\">\r\n      <div class=\"cell small-12 text-center margin-top-2\" [hidden]=\"examSimulationQuestionCollection.length\">\r\n        <i class=\"fa fa-spinner fa-spin fa-2x fa-fw\"></i>\r\n        <span class=\"sr-only\">Carregando...</span>\r\n      </div>\r\n      <div class=\"cell small-12 medium-8 medium-offset-2 text-center margin-top-1\" [hidden]=\"finish\">\r\n        <div class=\"grid-x\">\r\n          <div class=\"cell small-12 text-center margin-1\">\r\n            <h5>Responda as perguntas:</h5>\r\n          </div>\r\n        </div>\r\n        <div class=\"grid-x simulation-question\" *ngFor=\"let examSimulationQuestion of examSimulationQuestionCollection; let i = index\" [ngClass]=\"{'simulation-question__active': stepActive==i}\">\r\n          <div class=\"cell small-12 medium-10 medium-offset-1\">\r\n            <div class=\"grid-x\">\r\n              <div class=\"cell small-12 show-for-small-only text-center\">\r\n                {{ examSimulationQuestion.exam_simulation.exam.name }} - {{ examSimulationQuestion.exam_simulation.name }}\r\n              </div>\r\n              <div class=\"cell small-12 show-for-small-only text-center\">\r\n                {{ examSimulationQuestion.exam_simulation_question_discipline.name }}\r\n              </div>\r\n              <div class=\"cell medium-6 hide-for-small-only text-left\">\r\n                {{ examSimulationQuestion.exam_simulation.exam.name }} - {{ examSimulationQuestion.exam_simulation.name }}\r\n              </div>\r\n              <div class=\"cell medium-6 hide-for-small-only text-right\">\r\n                {{ examSimulationQuestion.exam_simulation_question_discipline.name }}\r\n              </div>\r\n            </div>\r\n            <hr />\r\n            <div class=\"grid-x grid-padding-x\">\r\n              <div class=\"cell small-12 medium-12 text-left\">\r\n                <article>\r\n                  <p>{{ examSimulationQuestion.question }}</p>\r\n                  <div class=\"simulation-question__answer padding-1\">\r\n                    <form [formGroup]=\"forms[i]\">\r\n                      <label *ngFor=\"let examSimulationQuestionAnswer of examSimulationQuestion.exam_simulation_question_answers; let iq = index\">\r\n                      <input type=\"radio\" formControlName=\"answer\" value=\"{{ iq }}\" />\r\n                        {{ examSimulationQuestionAnswer.answer }}\r\n                      </label>\r\n                    </form>\r\n                  </div>\r\n                </article>\r\n              </div>\r\n            </div>\r\n            <hr />\r\n            <div class=\"grid-x\">\r\n              <div class=\"cell small-12 show-for-small-only\">\r\n                <button type=\"button\" class=\"button expanded\" (click)=\"onFormSubmit(i)\" [disabled]=\"!forms[i].valid\">Próximo</button>\r\n              </div>\r\n              <div class=\"cell medium-12 hide-for-small-only text-right\">\r\n                <button type=\"button\" class=\"button\" (click)=\"onFormSubmit(i)\" [disabled]=\"!forms[i].valid\">Próximo</button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"grid-x\">\r\n          <div class=\"cell small-12 medium-10 medium-offset-1 text-left margin-top-2\">\r\n            <a [routerLink]=\"['']\">Sair do teste</a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"cell small-12 medium-8 medium-offset-2 margin-top-1\" [hidden]=\"!finish\">\r\n        <div class=\"grid-x\">\r\n          <div class=\"cell small-12 text-center\">\r\n            <h5>Parabéns, você terminou seu teste.</h5>\r\n            <p>Tempo total: {{ examResult.execTime }}</p>\r\n          </div>\r\n          <div class=\"cell small-12 medium-4 text-center\">\r\n            <p>Total de questões: {{ examResult.questions.total }}</p>\r\n          </div>\r\n          <div class=\"cell small-12 medium-4 text-center\">\r\n            <p>Total de acertos: {{ examResult.questions.hits }}</p>\r\n          </div>\r\n          <div class=\"cell small-12 medium-4 text-center\">\r\n            <p>Total de erros: {{ examResult.questions.falts }}</p>\r\n          </div>\r\n        </div>\r\n        <div class=\"grid-x\">\r\n          <div class=\"cell auto\">\r\n            <canvas baseChart [datasets]=\"barChartData\" [labels]=\"barChartLabels\" [options]=\"barChartOptions\" [legend]=\"barChartLegend\" [chartType]=\"barChartType\"></canvas>\r\n          </div>\r\n        </div>\r\n        <div class=\"grid-x\">\r\n          <div class=\"cell small-12 medium-10 medium-offset-1 text-right margin-top-2\">\r\n            <a [routerLink]=\"['']\">Ir para o menu principal</a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</main>\r\n"

/***/ }),

/***/ "../../../../../src/app/exam-simulation-question/exam-simulation-question.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".simulation-question {\n  display: none; }\n  .simulation-question.simulation-question__active {\n    display: block; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/exam-simulation-question/exam-simulation-question.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamSimulationQuestionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_services_exam_simulation_question_service__ = __webpack_require__("../../../../../src/app/shared/services/exam-simulation-question.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ExamSimulationQuestionComponent = (function () {
    function ExamSimulationQuestionComponent(activeRoute, ExamSimulationQuestionService) {
        this.activeRoute = activeRoute;
        this.ExamSimulationQuestionService = ExamSimulationQuestionService;
        this.forms = [];
        this.stepActive = 0;
        this.examResult = {
            execTime: '',
            questions: { total: 0, falts: 0, hits: 0 }
        };
        this.answersCollection = [];
        this.examSimulationQuestionCollection = [];
        this.startAt = new Date();
        this.finish = false;
        this.barChartType = 'bar';
        this.barChartLabels = [];
        this.barChartLegend = false;
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };
        this.barChartData = [
            { data: [], label: '' },
            { data: [], label: '' }
        ];
    }
    ExamSimulationQuestionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeSub = this.activeRoute.params.subscribe(function (params) {
            _this.ExamSimulationQuestionService
                .fetchAll(+params['examId'], +params['simulationId'])
                .subscribe(function (res) {
                _this.examSimulationQuestionCollection = res;
                for (var i in res) {
                    _this.forms[i] = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
                        answer: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["e" /* Validators */].required)
                    });
                }
                _this.barChartLabels = _this.examSimulationQuestionCollection.map(function (i) {
                    return i.exam_simulation_question_discipline.name;
                });
            });
        });
    };
    ExamSimulationQuestionComponent.prototype.onFormSubmit = function (index) {
        var form = this.forms[index];
        var question = this.examSimulationQuestionCollection[index];
        var answer = question.exam_simulation_question_answers[form.controls['answer'].value];
        answer['exam_simulation_question_discipline'] = question.exam_simulation_question_discipline;
        this.answersCollection.push(answer);
        if (index >= (this.forms.length - 1)) {
            this.examResult = {
                execTime: this.stopCountDown(),
                questions: {
                    total: this.examSimulationQuestionCollection.length,
                    falts: this.answersCollection.filter(function (i) { return !i.correct ? 1 : 0; }).length,
                    hits: this.answersCollection.filter(function (i) { return i.correct ? 1 : 0; }).length
                }
            };
            this.barChartData = [
                { label: 'Erradas', data: this.answersCollection.map(function (i) { return !i.correct ? 1 : 0; }) },
                { label: 'Corretas', data: this.answersCollection.map(function (i) { return i.correct ? 1 : 0; }) }
            ];
            this.finish = true;
        }
        this.stepActive++;
    };
    ExamSimulationQuestionComponent.prototype.stopCountDown = function () {
        var days, hours, minutes, seconds, t;
        t = (new Date().getTime() - this.startAt.getTime()) / 1000;
        days = Math.floor(t / 86400);
        t -= days * 86400;
        hours = Math.floor(t / 3600) % 24;
        t -= hours * 3600;
        minutes = Math.floor(t / 60) % 60;
        t -= minutes * 60;
        seconds = Math.round(t % 60);
        return [hours + 'h', minutes + 'm', seconds + 's'].join(' ');
    };
    ExamSimulationQuestionComponent.prototype.ngOnDestroy = function () {
        this.routeSub.unsubscribe();
    };
    ExamSimulationQuestionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'exam-simulation-question-page',
            template: __webpack_require__("../../../../../src/app/exam-simulation-question/exam-simulation-question.component.html"),
            styles: [__webpack_require__("../../../../../src/app/exam-simulation-question/exam-simulation-question.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__shared_services_exam_simulation_question_service__["a" /* ExamSimulationQuestionService */]])
    ], ExamSimulationQuestionComponent);
    return ExamSimulationQuestionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/exam-simulation-question/exam-simulation-question.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamSimulationQuestionModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_charts__ = __webpack_require__("../../../../ng2-charts/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__exam_simulation_question_component__ = __webpack_require__("../../../../../src/app/exam-simulation-question/exam-simulation-question.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared__ = __webpack_require__("../../../../../src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var examSimulationQuestionRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forChild([
    {
        path: ':examId/simulation/:simulationId',
        component: __WEBPACK_IMPORTED_MODULE_3__exam_simulation_question_component__["a" /* ExamSimulationQuestionComponent */]
    }
]);
var ExamSimulationQuestionModule = (function () {
    function ExamSimulationQuestionModule() {
    }
    ExamSimulationQuestionModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                examSimulationQuestionRouting,
                __WEBPACK_IMPORTED_MODULE_4__shared__["g" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_2_ng2_charts__["ChartsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__exam_simulation_question_component__["a" /* ExamSimulationQuestionComponent */]
            ]
        })
    ], ExamSimulationQuestionModule);
    return ExamSimulationQuestionModule;
}());



/***/ }),

/***/ "../../../../../src/app/exam-simulation/exam-simulation.component.html":
/***/ (function(module, exports) {

module.exports = "<main class=\"grid-container\">\r\n  <section>\r\n    <div class=\"grid-x\">\r\n      <div class=\"cell small-12 medium-4 medium-offset-4 text-center margin-top-3\">\r\n        <h5>Selecione o simulado que deseja fazer:</h5>\r\n      </div>\r\n      <div class=\"cell small-12 medium-4 medium-offset-4 text-center margin-top-1\">\r\n        <div class=\"grid-x\">\r\n          <div class=\"cell small-12 text-center margin-bottom-1\" [hidden]=\"examSimulationCollection.length\">\r\n            <i class=\"fa fa-spinner fa-spin fa-2x fa-fw\"></i>\r\n            <span class=\"sr-only\">Carregando...</span>\r\n          </div>\r\n          <div class=\"cell small-12 text-center\" *ngFor=\"let examSimulation of examSimulationCollection\">\r\n            <a [routerLink]=\"[examSimulation.id]\">{{ examSimulation.exam.name }} - {{ examSimulation.name }}</a>\r\n          </div>\r\n        </div>\r\n        <div class=\"grid-x\">\r\n          <div class=\"cell small-12 text-left\">\r\n            <a [routerLink]=\"['']\">Voltar</a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</main>\r\n"

/***/ }),

/***/ "../../../../../src/app/exam-simulation/exam-simulation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamSimulationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_services_exam_simulation_service__ = __webpack_require__("../../../../../src/app/shared/services/exam-simulation.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ExamSimulationComponent = (function () {
    function ExamSimulationComponent(route, ExamSimulationService) {
        this.route = route;
        this.ExamSimulationService = ExamSimulationService;
        this.examSimulationCollection = [];
    }
    ExamSimulationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeSub = this.route.params.subscribe(function (params) {
            _this.ExamSimulationService
                .fetchAll(+params['examId'])
                .subscribe(function (res) {
                _this.examSimulationCollection = res;
            });
        });
    };
    ExamSimulationComponent.prototype.ngOnDestroy = function () {
        this.routeSub.unsubscribe();
    };
    ExamSimulationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'exam-simulation-page',
            template: __webpack_require__("../../../../../src/app/exam-simulation/exam-simulation.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__shared_services_exam_simulation_service__["a" /* ExamSimulationService */]])
    ], ExamSimulationComponent);
    return ExamSimulationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/exam-simulation/exam-simulation.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamSimulationModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__exam_simulation_component__ = __webpack_require__("../../../../../src/app/exam-simulation/exam-simulation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("../../../../../src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var examSimulationRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forChild([
    {
        path: ':examId/simulation',
        component: __WEBPACK_IMPORTED_MODULE_2__exam_simulation_component__["a" /* ExamSimulationComponent */]
    }
]);
var ExamSimulationModule = (function () {
    function ExamSimulationModule() {
    }
    ExamSimulationModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                examSimulationRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["g" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__exam_simulation_component__["a" /* ExamSimulationComponent */]
            ]
        })
    ], ExamSimulationModule);
    return ExamSimulationModule;
}());



/***/ }),

/***/ "../../../../../src/app/exam/exam.component.html":
/***/ (function(module, exports) {

module.exports = "<main class=\"grid-container\">\r\n  <section>\r\n    <div class=\"grid-x\">\r\n      <div class=\"cell small-12 medium-4 medium-offset-4 text-center margin-top-3\">\r\n        <h5>Veja os testes disponíveis:</h5>\r\n      </div>\r\n      <div class=\"cell small-12 medium-4 medium-offset-4 text-center margin-top-1\">\r\n        <div class=\"grid-x\">\r\n          <div class=\"cell small-12 text-center margin-bottom-1\" [hidden]=\"examCollection.length\">\r\n            <i class=\"fa fa-spinner fa-spin fa-2x fa-fw\"></i>\r\n            <span class=\"sr-only\">Carregando...</span>\r\n          </div>\r\n          <div class=\"cell small-12 text-center\" *ngFor=\"let exam of examCollection\">\r\n            <a [routerLink]=\"[exam.id, 'simulation']\">{{ exam.name }}</a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </section>\r\n</main>\r\n"

/***/ }),

/***/ "../../../../../src/app/exam/exam.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_services_exam_service__ = __webpack_require__("../../../../../src/app/shared/services/exam.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExamComponent = (function () {
    function ExamComponent(ExamService) {
        this.ExamService = ExamService;
        this.examCollection = [];
    }
    ExamComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ExamService
            .fetchAll()
            .subscribe(function (res) {
            _this.examCollection = res;
        });
    };
    ExamComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'exam-page',
            template: __webpack_require__("../../../../../src/app/exam/exam.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_services_exam_service__["a" /* ExamService */]])
    ], ExamComponent);
    return ExamComponent;
}());



/***/ }),

/***/ "../../../../../src/app/exam/exam.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__exam_component__ = __webpack_require__("../../../../../src/app/exam/exam.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared__ = __webpack_require__("../../../../../src/app/shared/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var examRouting = __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forChild([
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__exam_component__["a" /* ExamComponent */]
    }
]);
var ExamModule = (function () {
    function ExamModule() {
    }
    ExamModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                examRouting,
                __WEBPACK_IMPORTED_MODULE_3__shared__["g" /* SharedModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__exam_component__["a" /* ExamComponent */]
            ]
        })
    ], ExamModule);
    return ExamModule;
}());



/***/ }),

/***/ "../../../../../src/app/shared/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__layout__ = __webpack_require__("../../../../../src/app/shared/layout/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__layout__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_0__layout__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models__ = __webpack_require__("../../../../../src/app/shared/models/index.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/shared/services/index.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__services__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__services__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_2__services__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_2__services__["d"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_3__shared_module__["a"]; });






/***/ }),

/***/ "../../../../../src/app/shared/layout/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer class=\"hide-for-small-only\">\r\n  <div class=\"grid-x\">\r\n    <div class=\"cell auto text-center padding-1\">\r\n      Studos © 2017 - Todos os direitos reservados.\r\n    </div>\r\n  </div>\r\n</footer>\r\n"

/***/ }),

/***/ "../../../../../src/app/shared/layout/footer.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "footer {\n  position: fixed;\n  bottom: 0;\n  margin-bottom: 0;\n  width: 100%;\n  color: #fff;\n  background-color: #333;\n  font-size: 0.8rem; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/layout/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'layout-footer',
            template: __webpack_require__("../../../../../src/app/shared/layout/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/layout/footer.component.scss")]
        })
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/shared/layout/header.component.html":
/***/ (function(module, exports) {

module.exports = "<header>\r\n  <div class=\"grid-x\">\r\n    <div class=\"cell auto text-left padding-1\">\r\n      <a href=\"https://studos.com.br/\" title=\"Studos\" target=\"_blank\">Studos</a>\r\n    </div>\r\n  </div>\r\n</header>\r\n"

/***/ }),

/***/ "../../../../../src/app/shared/layout/header.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "header {\n  background-color: #00af00; }\n  header a {\n    color: #fff;\n    font-weight: bold;\n    font-style: 1.4rem; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/layout/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var HeaderComponent = (function () {
    function HeaderComponent() {
    }
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'layout-header',
            template: __webpack_require__("../../../../../src/app/shared/layout/header.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/layout/header.component.scss")]
        })
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/shared/layout/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__footer_component__ = __webpack_require__("../../../../../src/app/shared/layout/footer.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__footer_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__header_component__ = __webpack_require__("../../../../../src/app/shared/layout/header.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__header_component__["a"]; });




/***/ }),

/***/ "../../../../../src/app/shared/models/exam-simulation-question-answer.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ExamSimulationQuestionAnswer */
var ExamSimulationQuestionAnswer = (function () {
    function ExamSimulationQuestionAnswer(id, answer, correct, createdby, updatedby, createdat, updatedat, enabled) {
        this.id = id;
        this.answer = answer;
        this.correct = correct;
        this.createdby = createdby;
        this.updatedby = updatedby;
        this.createdat = createdat;
        this.updatedat = updatedat;
        this.enabled = enabled;
    }
    return ExamSimulationQuestionAnswer;
}());



/***/ }),

/***/ "../../../../../src/app/shared/models/exam-simulation-question-discipline.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ExamSimulationQuestionDiscipline */
var ExamSimulationQuestionDiscipline = (function () {
    function ExamSimulationQuestionDiscipline(id, name, createdby, updatedby, createdat, updatedat, enabled) {
        this.id = id;
        this.name = name;
        this.createdby = createdby;
        this.updatedby = updatedby;
        this.createdat = createdat;
        this.updatedat = updatedat;
        this.enabled = enabled;
    }
    return ExamSimulationQuestionDiscipline;
}());



/***/ }),

/***/ "../../../../../src/app/shared/models/exam-simulation-question.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ExamSimulationQuestion */
var ExamSimulationQuestion = (function () {
    function ExamSimulationQuestion(id, question, answer_type, createdby, updatedby, createdat, updatedat, enabled, exam_simulation_question_discipline, exam_simulation_question_answers) {
        this.id = id;
        this.question = question;
        this.answer_type = answer_type;
        this.createdby = createdby;
        this.updatedby = updatedby;
        this.createdat = createdat;
        this.updatedat = updatedat;
        this.enabled = enabled;
        this.exam_simulation_question_discipline = exam_simulation_question_discipline;
        this.exam_simulation_question_answers = exam_simulation_question_answers;
    }
    return ExamSimulationQuestion;
}());



/***/ }),

/***/ "../../../../../src/app/shared/models/exam-simulation.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ExamSimulation */
var ExamSimulation = (function () {
    function ExamSimulation(id, name, version, createdby, updatedby, createdat, updatedat, enabled) {
        this.id = id;
        this.name = name;
        this.version = version;
        this.createdby = createdby;
        this.updatedby = updatedby;
        this.createdat = createdat;
        this.updatedat = updatedat;
        this.enabled = enabled;
    }
    return ExamSimulation;
}());



/***/ }),

/***/ "../../../../../src/app/shared/models/exam.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Exam */
var Exam = (function () {
    function Exam(id, name, createdby, updatedby, createdat, updatedat, enabled) {
        this.id = id;
        this.name = name;
        this.createdby = createdby;
        this.updatedby = updatedby;
        this.createdat = createdat;
        this.updatedat = updatedat;
        this.enabled = enabled;
    }
    return Exam;
}());



/***/ }),

/***/ "../../../../../src/app/shared/models/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__exam_model__ = __webpack_require__("../../../../../src/app/shared/models/exam.model.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__exam_simulation_model__ = __webpack_require__("../../../../../src/app/shared/models/exam-simulation.model.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__exam_simulation_question_model__ = __webpack_require__("../../../../../src/app/shared/models/exam-simulation-question.model.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__exam_simulation_question_answer_model__ = __webpack_require__("../../../../../src/app/shared/models/exam-simulation-question-answer.model.ts");
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__exam_simulation_question_discipline_model__ = __webpack_require__("../../../../../src/app/shared/models/exam-simulation-question-discipline.model.ts");
/* unused harmony namespace reexport */







/***/ }),

/***/ "../../../../../src/app/shared/services/exam-simulation-question.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamSimulationQuestionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__http_client_service__ = __webpack_require__("../../../../../src/app/shared/services/http-client.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ExamSimulationQuestionService = (function () {
    function ExamSimulationQuestionService(httpClientService) {
        this.httpClientService = httpClientService;
    }
    ExamSimulationQuestionService.prototype.fetchAll = function (examId, simulationId) {
        return this.httpClientService.get("/v1/exam/" + examId + "/simulation/" + simulationId + "/question")
            .map(function (res) { return res; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["a" /* Observable */].throw(error.message || 'Server error'); });
    };
    ExamSimulationQuestionService.prototype.fetchOne = function (examId, simulationId, questionId) {
        return this.httpClientService.get("/v1/exam/" + examId + "/simulation/" + simulationId + "/question/" + questionId)
            .map(function (res) { return res; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["a" /* Observable */].throw(error.message || 'Server error'); });
    };
    ExamSimulationQuestionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__http_client_service__["a" /* HttpClientService */]])
    ], ExamSimulationQuestionService);
    return ExamSimulationQuestionService;
}());



/***/ }),

/***/ "../../../../../src/app/shared/services/exam-simulation.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamSimulationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__http_client_service__ = __webpack_require__("../../../../../src/app/shared/services/http-client.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ExamSimulationService = (function () {
    function ExamSimulationService(httpClientService) {
        this.httpClientService = httpClientService;
    }
    ExamSimulationService.prototype.fetchAll = function (examId) {
        return this.httpClientService.get("/v1/exam/" + examId + "/simulation/")
            .map(function (res) { return res; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["a" /* Observable */].throw(error.message || 'Server error'); });
    };
    ExamSimulationService.prototype.fetchOne = function (examId, simulationId) {
        return this.httpClientService.get("/v1/exam/" + examId + "/simulation/" + simulationId)
            .map(function (res) { return res; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["a" /* Observable */].throw(error.message || 'Server error'); });
    };
    ExamSimulationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__http_client_service__["a" /* HttpClientService */]])
    ], ExamSimulationService);
    return ExamSimulationService;
}());



/***/ }),

/***/ "../../../../../src/app/shared/services/exam.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExamService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__http_client_service__ = __webpack_require__("../../../../../src/app/shared/services/http-client.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ExamService = (function () {
    function ExamService(httpClientService) {
        this.httpClientService = httpClientService;
    }
    ExamService.prototype.fetchAll = function () {
        return this.httpClientService.get('/v1/exam/')
            .map(function (res) { return res; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["a" /* Observable */].throw(error.message || 'Server error'); });
    };
    ExamService.prototype.fetchOne = function (examId) {
        return this.httpClientService.get("/v1/exam/" + examId)
            .map(function (res) { return res; })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["a" /* Observable */].throw(error.message || 'Server error'); });
    };
    ExamService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__http_client_service__["a" /* HttpClientService */]])
    ], ExamService);
    return ExamService;
}());



/***/ }),

/***/ "../../../../../src/app/shared/services/http-client.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpClientService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HttpClientService = (function () {
    function HttpClientService(http) {
        this.http = http;
    }
    HttpClientService.prototype.setHeaders = function () {
        var headersConfig = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Request-Width, Content-Type, Accept'
        };
        return new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */](headersConfig);
    };
    HttpClientService.prototype.formatErrors = function (error) {
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["a" /* Observable */].throw(error.json());
    };
    HttpClientService.prototype.get = function (path, params) {
        if (params === void 0) { params = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* URLSearchParams */](); }
        return this.http.get("" + __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].api_base_uri + path, { headers: this.setHeaders(), search: params })
            .catch(this.formatErrors)
            .map(function (res) { return res.json(); });
    };
    HttpClientService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], HttpClientService);
    return HttpClientService;
}());



/***/ }),

/***/ "../../../../../src/app/shared/services/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_client_service__ = __webpack_require__("../../../../../src/app/shared/services/http-client.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__http_client_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__exam_service__ = __webpack_require__("../../../../../src/app/shared/services/exam.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__exam_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__exam_simulation_service__ = __webpack_require__("../../../../../src/app/shared/services/exam-simulation.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_2__exam_simulation_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__exam_simulation_question_service__ = __webpack_require__("../../../../../src/app/shared/services/exam-simulation-question.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_3__exam_simulation_question_service__["a"]; });






/***/ }),

/***/ "../../../../../src/app/shared/shared.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SharedModule = (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* RouterModule */]
            ],
            declarations: [],
            exports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* RouterModule */],
            ]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: false,
    api_base_uri: 'https://studos-services.meggie.co'
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map